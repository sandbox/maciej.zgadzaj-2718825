<?php
/**
 * @file
 * Adds 'Allowed values function' element to list field settings form.
 */

/**
 * Implements hook_form_alter().
 *
 * @see field_ui_field_settings_form()
 * @see field_ui_field_edit_form()
 */
function form_list_allowed_values_function_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'field_ui_field_settings_form' || $form_id == 'field_ui_field_edit_form') {
    // Only if 'allowed_values_function' field setting already exists -
    // so that we add our modifications for the relevant fields only.
    if (!empty($form['field']['settings']['allowed_values_function'])) {

      // By default this form element exists, it is just of type 'value'.
      // Let's then create a normal 'textfield' in its place.
      $form['field']['settings']['allowed_values_function'] = array(
        '#type' => 'textfield',
        '#title' => t('Allowed values function'),
        '#default_value' => $form['field']['settings']['allowed_values_function']['#value'],
        // Display the 'Allowed values list' field only if
        // 'Allowed values function' field is empty.
        '#states' => array(
          'invisible' => array(
            ':input[name="field[settings][allowed_values]"]' => array('empty' => FALSE),
          ),
        ),
        '#element_validate' => array('form_list_allowed_values_function_form_element_validate'),
      );

      // When editing an existing field which already uses a callback function
      // for its values, the message 'The value of this field is being determined
      // by the ... function and may not be changed.' is displayed. As we now
      // allow to change both the function as well as the values, obviously
      // we do not want this message to be displayed there anymore.
      if (isset($form['field']['settings']['allowed_values_function_display'])) {
        unset($form['field']['settings']['allowed_values_function_display']);
      }

      // When a value callback function is used, the access to the custom values
      // list field is forbidden. In our case, let's still allow it.
      $form['field']['settings']['allowed_values']['#access'] = TRUE;

      // Display the 'Allowed values function' field only if
      // 'Allowed values list' field is empty.
      $form['field']['settings']['allowed_values']['#states'] = array(
        'invisible' => array(
          ':input[name="field[settings][allowed_values_function]"]' => array('empty' => FALSE),
        ),
      );

    }
  }
}

/**
 * Validates the allowed_values_function form element.
 *
 * @see form_list_allowed_values_function_form_alter()
 */
function form_list_allowed_values_function_form_element_validate($element, &$form_state) {
  if (!empty($element['#value']) && !function_exists($element['#value'])) {
    form_error($element, t('Function %function does not exist.', array('%function' => $element['#value'])));
  }
}
